#!/usr/bin/env bash
py3m() {
	python3 -m "$@"
}

pipenv() {
	py3m pipenv "$@"
}

