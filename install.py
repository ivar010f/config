#!/usr/bin/env python3
"""Install shell environment for bash and fish."""
import logging
import os
import pathlib
import subprocess

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

THIS = pathlib.Path(__file__).parent.absolute()
HOME = pathlib.Path(os.environ["HOME"])


def main():
    _link("git/.gitconfig")

    _link_config("bash")
    _append_bash_profile("source ~/.config/bash/profile.bash")

    _link_config("fish")
    _link_config("omf")
    _install_pyenv()


def _link(source, target_dir=None):
    full_source = THIS / source
    full_target = HOME / (target_dir or ".")

    full_target.mkdir(parents=True, exist_ok=True)
    _symlink(full_source, full_target)


def _symlink(source, target):
    log.info("Creating symlink %s -> %s", source, target)
    base_cmd = "ln --symbolic --backup=numbered --relative --verbose"
    cmd_args_fmt = "--target-directory={target} -- {source}"
    cmd_args = cmd_args_fmt.format(target=target, source=source)
    _process((*base_cmd.split(), *cmd_args.split()))


def _link_config(source, target_dir=None):
    config_target_dir = pathlib.Path(".config") / (target_dir or ".")
    _link(source, config_target_dir)


def _append_bash_profile(line):
    _append_line_if_not_in_file(line, HOME / ".bash_profile")


def _append_line_if_not_in_file(line, path):
    line_with_break = line.strip() + "\n"
    indicator = line_with_break.splitlines()[0] + "\n"

    def is_in_file():
        with open(path) as f:
            return indicator in f

    def append_line():
        with open(path, "a") as f:
            f.write(line_with_break)

    if not is_in_file():
        append_line()


def _install_pyenv():
    pyenv_home = HOME / ".pyenv"
    if pyenv_home.exists():
        return
    _process(
        (*("git clone https://github.com/pyenv/pyenv.git".split()),
         str(pyenv_home))
    )


def _sh(command):
    subprocess.check_call(command, shell=True)


def _process(command):
    subprocess.check_call(command, shell=False)


if __name__ == "__main__":
    main()
