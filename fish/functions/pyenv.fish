#!/usr/bin/env fish
# Activate pyenv shims
set -x PYENV_ROOT $HOME/.pyenv
set -x PATH $PYENV_ROOT/bin $PATH
if command -v pyenv; pyenv init - | source; end

