# Auto-install fisher package manager
if not functions -q fisher
    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

# Manual install of OMF
if not functions -q omf
    curl -L https://get.oh-my.fish | fish
end

source $HOME/.config/fish/functions/python.fish

set EDITOR vim
